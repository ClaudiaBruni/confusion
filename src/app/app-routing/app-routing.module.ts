import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { routes } from './routes';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }

/*DOMANDE
1) non mi crea il file spec qui
2) @import '../node_modules/font-awesome/css/font-awesome.css'; è diverso da quello che mette lui
*/
