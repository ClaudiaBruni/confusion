import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
import { DISHES } from '../shared/dishes'

@Injectable({
  providedIn: 'root'
})
export class DishService {

  constructor() { }

  getDishes(): Dish[]{
    return DISHES;
  }

  getDish(id: string): Dish{
    return DISHES.filter((dish) => dish.id === id)[0];
    /*se non mettessi [0] avrei sempre selezionato un unico risultato ma sarebbe sottoforma di array */
  }

  getFeaturedDish(): Dish{
    return DISHES.filter((dish) => dish.featured)[0];
  }
}
